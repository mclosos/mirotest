FROM ubuntu:20.04
RUN apt-get update
RUN apt-get install -y gnupg2 \
    software-properties-common \
    wget
RUN wget -O- https://apt.corretto.aws/corretto.key | apt-key add -
RUN add-apt-repository 'deb https://apt.corretto.aws stable main'
RUN apt update
RUN apt-get install -y \
    gnupg2 \
    java-common \
    java-11-amazon-corretto-jdk \
    libncurses5-dev \
    libxml2-dev \
    libxmlsec1-dev \
    libxslt-dev \
    pkg-config \
    libpq-dev \
    unzip \
    build-essential \
    && rm -rf /var/lib/apt/lists/*
ENV GRADLE_VERSION=6.8.3
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
RUN mv gradle-${GRADLE_VERSION}-bin.zip /tmp
RUN unzip -d /opt/gradle /tmp/gradle-${GRADLE_VERSION}-bin.zip
RUN ln -s /opt/gradle/gradle-${GRADLE_VERSION} /opt/gradle/latest
ENV DISPLAY :10
ENV LANG C.UTF-8
ENV JAVA_HOME=/usr/lib/jvm/java-11-amazon-corretto
ENV GRADLE_HOME=/opt/gradle/latest
COPY ${PWD} "/www/miro-test"
WORKDIR "/www/miro-test"