package com.example.pages;

import com.example.core.AppContainer;
import com.example.core.ConfigReader;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {

    private final WebDriverWait wait;
    protected final AppContainer app;

    public BasePage(AppContainer app) {
        this.app = app;
        wait = new WebDriverWait(app.driver, 10);
        PageFactory.initElements(app.driver, this);
    }

    // TODO: Implement check page loaded method

    protected void waitElementClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitElementVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void clickElement(WebElement element) {
        waitElementClickable(element);
        element.click();
    }

    public void inputText(WebElement element, String text) {
        waitElementVisible(element);
        element.click();
        element.sendKeys(text);
    }

    public String getElementText(WebElement element) {
        waitElementVisible(element);
        return element.getText();
    }

    public void openPageByUrl(String url) {
        app.driver.get(ConfigReader.getProperty("baseURL").concat(url));
    }
}
