package com.example.pages;

import com.example.core.AppContainer;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class DashboardPage extends BasePage {

    @FindBy(css = ".user-profile__button")
    private WebElement userProfileIcon;
    @FindBy(css = ".user-profile__profile-info .user-profile__text--email")
    private WebElement userEmail;

    public DashboardPage(AppContainer app) {
        super(app);
    }

    @Step("Click user profile icon")
    public void clickUserProfileIcon() {
        waitElementClickable(userProfileIcon);
        clickElement(userProfileIcon);
    }

    @Step("Get user email value")
    public String getUserEmailText() {
        waitElementVisible(userEmail);
        return userEmail.getText();
    }
}
