package com.example.pages;

import com.example.core.AppContainer;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SignInPage extends BasePage {

    @FindBy(css = "[data-autotest-id=\"mr-form-login-email-1\"]")
    private WebElement emailField;
    @FindBy(css = "[data-autotest-id=\"mr-form-login-password-1\"]")
    private WebElement passwordField;
    @FindBy(css = "[data-autotest-id=\"mr-form-login-btn-signin-1\"]")
    private WebElement signInButton;
    @FindBy(css = "[data-autotest-id=\"mr-link-forgot-password-1\"]")
    private WebElement forgotPasswordLink;
    @FindBy(css = "[data-autotest-id=\"mr-error-please enter your email address.-1\"]")
    private WebElement enterEmailError;
    @FindBy(css = "[data-autotest-id=\"mr-error-please enter your password.-1\"]")
    private WebElement enterPasswordError;
    @FindBy(css = ".signup__error")
    private WebElement signInError;

    public SignInPage(AppContainer app) {
        super(app);
    }

    @Step("Open page url")
    public void openPage() {
        openPageByUrl("/login");
    }

    @Step("Input email {email}")
    public void inputEmail(String email) {
        inputText(emailField, email);
    }

    @Step("Input password {password}")
    public void inputPassword(String password) {
        clickElement(passwordField);
        inputText(passwordField, password);
    }

    @Step("Click Sign in button")
    public void clickSignInButton() {
        clickElement(signInButton);
    }

    @Step("Click Forgot password link")
    public void clickForgotPasswordLink() {
        clickElement(forgotPasswordLink);
    }

    @Step("Get email input error text")
    public String getEmailErrorText() {
        return getElementText(enterEmailError);
    }

    @Step("Get empty password error text")
    public String getEmptyPasswordErrorText() {
        return getElementText(enterPasswordError);
    }

    @Step("Get wrong email input error text")
    public String getWrongEmailPasswordErrorText() {
        return getElementText(signInError);
    }
}
