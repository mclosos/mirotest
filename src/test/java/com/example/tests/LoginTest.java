package com.example.tests;

import com.example.core.ConfigReader;
import com.example.pages.DashboardPage;
import com.example.pages.SignInPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class LoginTest extends BaseTest {
    public static SignInPage signInPage;
    public static DashboardPage dashboardPage;

    @Test
    @Feature("Sign in")
    @Story("Positive Sign in scenario")
    public void SignInPositiveTest() {
        signInPage = new SignInPage(app);
        dashboardPage = new DashboardPage(app);
        signInPage.openPage();
        signInPage.inputEmail(ConfigReader.getProperty("testEmail"));
        signInPage.inputPassword(ConfigReader.getProperty("testPassword"));
        signInPage.clickSignInButton();
        dashboardPage.clickUserProfileIcon();
        assertEquals(ConfigReader.getProperty("testEmail"), dashboardPage.getUserEmailText());
    }

    @Test
    @Feature("Sign in")
    @Story("Sign in attempt with empty credentials")
    public void SignInEmptyCredentialsTest() {
        signInPage = new SignInPage(app);
        signInPage.openPage();
        signInPage.clickSignInButton();
        assertEquals("Please enter your email address.", signInPage.getEmailErrorText());
        assertEquals("Please enter your password.", signInPage.getEmptyPasswordErrorText());
    }

    @Test
    @Feature("Sign in")
    @Story("Sign in attempt with empty password field")
    public void SignInEmptyEmailTest() {
        signInPage = new SignInPage(app);
        signInPage.openPage();
        signInPage.inputEmail(ConfigReader.getProperty("testEmail"));
        signInPage.clickSignInButton();
        assertEquals("Please enter your password.", signInPage.getEmptyPasswordErrorText());
    }

    @Test
    @Feature("Sign in")
    @Story("Sign in attempt with empty email field")
    public void SignInEmptyPasswordTest() {
        signInPage = new SignInPage(app);
        signInPage.openPage();
        signInPage.inputPassword(ConfigReader.getProperty("testPassword"));
        signInPage.clickSignInButton();
        assertEquals("Please enter your email address.", signInPage.getEmailErrorText());
    }

    @Test
    @Feature("Sign in")
    @Story("Sign in attempt with wrong password")
    public void SignInIncorrectPasswordTest() {
        signInPage = new SignInPage(app);
        dashboardPage = new DashboardPage(app);
        signInPage.openPage();
        signInPage.inputEmail(ConfigReader.getProperty("testEmail"));
        signInPage.inputPassword(ConfigReader.getProperty("falsePassword"));
        signInPage.clickSignInButton();
        assertEquals("The email or password you entered is incorrect.\n" + "Please try again.",
                signInPage.getWrongEmailPasswordErrorText());
    }
}
