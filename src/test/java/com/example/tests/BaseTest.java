package com.example.tests;

import com.example.core.AppContainer;
import com.example.core.LocalApp;
import com.example.core.RemoteApp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;


public class BaseTest {
    public AppContainer app;

    @BeforeEach
    public void setUp() {
        if (System.getenv("DRIVER").equals("local")) {
            app = new LocalApp("chromedriver");

        } else if (System.getenv("DRIVER").equals("remote")) {

            String browser = System.getenv("BROWSER");
            String version = System.getenv("VERSION");
            app = new RemoteApp(browser, version);
        }
    }

    @AfterEach
    public void tearDown() {
        assert app != null;
        app.driverClose();
    }
}
