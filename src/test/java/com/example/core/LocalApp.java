package com.example.core;

import org.openqa.selenium.chrome.ChromeDriver;

public class LocalApp extends AppContainer {
    public LocalApp(String driverType) {
        System.setProperty("webdriver.chrome.driver", ConfigReader.getProperty(driverType));
        if (driverType.equals("chromedriver")) {
            driver = new ChromeDriver();
            setResolution();
            setImplicitlyWait(10L);
        } else {
            throw new UnsupportedOperationException(String.format("%s not implemented yet", driverType));
        }
    }
}
