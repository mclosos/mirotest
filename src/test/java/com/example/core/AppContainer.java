package com.example.core;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class AppContainer {
    public WebDriver driver;

    public void setResolution() {
        // TODO: Implement browser resolution management
        driver.manage().window().maximize();
    }

    public void setImplicitlyWait(Long time) {
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }

    public void driverClose() {
        clearDriverCache(driver);
        driver.close();
        driver.quit();
    }

    private void clearDriverCache(WebDriver driver) {
        driver.manage().deleteAllCookies();
    }
}
