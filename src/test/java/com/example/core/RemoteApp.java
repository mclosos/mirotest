package com.example.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Map;

public class RemoteApp extends AppContainer {
    public RemoteApp(String browser, String version) {
        DesiredCapabilities options;
        if (browser.equals("chrome")) {
            new DesiredCapabilities();
            options = DesiredCapabilities.chrome();
        } else if (browser.equals("firefox")) {
            new DesiredCapabilities();
            options = DesiredCapabilities.firefox();
        } else {
            throw new ValueException("Need to specify BROWSER env variable");
        }

        JsonNode capabilities = readJsonCapabilities(
                ConfigReader.getProperty("capabilitiesPath")).get(browser).get(version);

        setCapabilitiesToOptions(capabilities, options);

        try {
            driver = new RemoteWebDriver(new URL(ConfigReader.getProperty("seleniumServer")), options);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        setResolution();
        setImplicitlyWait(10L);
    }


    private JsonNode readJsonCapabilities(String filepath) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        try {
            rootNode = mapper.readTree(Paths.get(filepath).toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rootNode;
    }

    private void setCapabilitiesToOptions(JsonNode capabilities, DesiredCapabilities options) {
        String browserName = capabilities.get("browserName").toString().replaceAll("^\"+|\"+$", "");
        String browserVersion = capabilities.get("version").toString().replaceAll("^\"+|\"+$", "");
        String pageLoadStrategy = capabilities.get("pageLoadStrategy").toString().replaceAll("^\"+|\"+$", "");
        Boolean acceptInsecureCerts = capabilities.get("acceptInsecureCerts").asBoolean();
        Boolean enableVNC = capabilities.get("enableVNC").asBoolean();
        Boolean acceptsSslCerts = capabilities.get("acceptSslCerts").asBoolean();
        Boolean enableVideo = capabilities.get("enableVideo").asBoolean();

        options.setCapability("name", "MiroTest");
        options.setCapability("browserName", browserName);
        options.setCapability("browserVersion", browserVersion);
        options.setCapability("selenoid:options", Map.<String, Object>of(
                "enableVNC", enableVNC,
                "enableVideo", enableVideo));
        options.setCapability("acceptInsecureCerts", acceptInsecureCerts);
        options.setCapability("acceptSslCerts", acceptsSslCerts);
        options.setCapability("pageLoadStrategy", pageLoadStrategy);
    }

}
