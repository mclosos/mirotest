# MIRO TEST
## JAVA + JUNIT + Selenium project

## ToDo List
- [X] Implement gradle build
- [X] Local driver running
- [X] Page object
- [X] Authorization test suite
- [X] Allure
- [X] Waits
- [X] Remote driver running
- [ ] Parallel running
- [X] Documentation
- [X] Dockerize it
- [ ] Ensure on page checks
- [ ] Mobile web testing
- [ ] Logging
- [ ] docker-compose

## Requirements

- Used [Amazon Corretto 11 SDK](https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html). 
  It should work with any Java SDK 9+
  
- Project can be built with [gradle 6.8.3](https://docs.gradle.org/6.8.3/userguide/installation.html).

## How does it work?

### Tests uses three environment variables:

- **DRIVER** - can be "local" (for local debug) and "remote" (to use remote selenium server);
- **BROWSER** - specifies browser name like "chrome", "firefox". We can add more options.
- **VERSION** - specifies browser major version like 78, 84 etc. We can add more options.

### They also use [config.properties](src/test/resources/config.properties) file to specify project options:

- **chromedriver** - specifies path to local chromedriver (for local development)
- **seleniumServer** - specifies url of Selenium server
- **baseUrl** - base url of web app
- **testEmail** - we don't have preconditions, so we use preregistered user
- **testPassword** - we don't have preconditions, so we use fixed password
- **falsePassword** - for negative scenarios we use fixed wrong password (which surely do not much the real one)
- **capabilitiesPath** - path to capabilities json. This file specifies available to run remote drivers.

### To run the one test for remote driver

```shell
export DRIVER=remote BROWSER=chrome VERSION=84 && ./gradlew test --tests "com.example.tests.LoginTest.SignInEmptyCredentialsTest"
```

### To run the one test for local driver

```shell
export DRIVER=local && ./gradlew test --tests "com.example.tests.LoginTest.SignInEmptyCredentialsTest"
```

### To run tests in Docker
1) Specify the host of selenium server in [config.properties](src/test/resources/config.properties)
```   
seleniumServer = http://192.168.0.1:4444/wd/hub
```
2) Build the container
```
docker build -t mirotest .
```
3) Run in console
```
docker run -e DRIVER=remote -e BROWSER=chrome -e VERSION=84 -it mirotest_miro-test ./gradlew test
```